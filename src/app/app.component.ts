import { Component } from '@angular/core';
import { AppConfigService } from './config/app.config';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  ENV: any = {};
  status: any = "Not Connected to Backend";
  constructor(
    private appConfig: AppConfigService,
    private http: HttpClient
  ) {
    this.ENV = this.appConfig.getConfig();

    this.checkBackendService()
    .subscribe(
      (data: any) => {
        this.status = data;
      }, 
      error => {
        this.status = "Not Connected to Backend";
      } 
    );

  }

  checkBackendService() {
    return this.http.get("http://"+this.ENV.backend_url,{
      responseType:"text"
    });
  }
}
