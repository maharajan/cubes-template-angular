import { Injectable } from '@angular/core';
import { HttpClient, HttpBackend } from '@angular/common/http';

@Injectable()
export class AppConfigService {
  private http: HttpClient;
  private _appConfig: Object = null;

  constructor(handler: HttpBackend) {
    // Avoids the interceptors so that we can load the App Config without authentication headers.
    this.http = new HttpClient(handler);
  }

  load() {
    return this.http.get('/assets/environment.json?v=' + Math.random())
      .toPromise()
      .then((config) => {
        this._appConfig = config;
      });
  }

  getConfig(): Object {
    return this._appConfig;
  }

  getConfigValue(key: string) {
    return this._appConfig ? this._appConfig[key] : null;
  }
}
