import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AppConfigService } from './config/app.config';
import { HttpClientModule } from '@angular/common/http';

const appInitializerFn = (appConfig: AppConfigService) => {
  return () => {
    return appConfig.load();
  };
};


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [ 
    AppConfigService,
    {
    provide: APP_INITIALIZER,
    useFactory: appInitializerFn,
    deps: [AppConfigService],
    multi: true
    }],
    
  bootstrap: [AppComponent],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class AppModule { }
