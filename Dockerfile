FROM nginx

# Node dependencies [Until multi-stage build is fixed]
RUN apt-get update
RUN apt-get install -y curl build-essential
RUN curl -sL https://deb.nodesource.com/setup_10.x | bash -
RUN apt-get install -y nodejs

WORKDIR /angular-ui
COPY ./package.json /angular-ui
RUN npm install
COPY . /angular-ui
RUN npm run build:prod
RUN mv ./dist/my-app/* /usr/share/nginx/html/

COPY ./entrypoint.sh /usr/share/nginx
RUN chmod +x /usr/share/nginx/entrypoint.sh
ENTRYPOINT ["/usr/share/nginx/entrypoint.sh"]
CMD ["nginx", "-g", "daemon off;"]
EXPOSE 80

## Stage 1: Build angular app
# FROM node:10 as builder
# WORKDIR /angular-ui
# COPY ./package.json /angular-ui
# RUN npm install

# COPY . /angular-ui
# RUN npm run build:prod

# ##stage 2: run nginx to serve application
# FROM nginx
# COPY --from=builder /angular-ui/dist/my-app /usr/share/nginx/html/
# EXPOSE 80